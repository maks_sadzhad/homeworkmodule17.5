#include <iostream>
#include <string>

using namespace std;

class Person
{
public:
	Person(string n, int a) 
	{
		name = n;
		age = a;
	}
	string GetName()
	{
		return name;
	}
	int GetAge()
	{
		return age;
	}
private:
	string name;
	int age;
};

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	double Show()
	{
		return sqrt(x * x + y * y + z * z);
	}

private:
		double x;
		double y;
		double z;
};



int main()
{
	Person p("Karl", 20);
	cout << "Name: " << p.GetName() << "\tAge: " << p.GetAge() << endl;
	Vector v(3, 4, 5);
    cout << "The length of the vector: " << v.Show() << endl;
}
	